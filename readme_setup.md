## Database  
```smartyconfig
-- For the Main App
$ mysql -uroot -e "drop schema if exists multi_tenancy; create schema \`multi_tenancy\` default character set utf8mb4 collate utf8mb4_unicode_ci;"

-- For a test Tenant
$ mysql -uroot -e "drop schema if exists multi_tenancy_test; create schema \`multi_tenancy_test\` default character set utf8mb4 collate utf8mb4_unicode_ci;"
```  

## Adding Example Tenants to Main App
```mysql
INSERT INTO `tenants` (`id`, `domain_name`, `tenant_name`, `db_host`, `db_name`, `db_user`, `db_password`, `db_port`, `db_charset`, `db_collation`, `timezone`, `created_at`, `updated_at`) VALUES
(1, 'test', 'Test', '127.0.0.1', 'multi_tenancy_test', 'root', '', '3306', 'utf8', 'utf8_unicode_ci', 'Asia/Kolkata', NULL, NULL),
(2, 'test1', 'Test 1', '127.0.0.1', 'multi_tenancy_test1', 'root', '', '3306', 'utf8', 'utf8_unicode_ci', 'Asia/Kolkata', NULL, NULL);
```

## Cache Driver
Make sure Redis is setup in the system. Since the App's uses it as the default cache driver instead of file.  

Redis was installed onto the project using `composer require  predis/predis`

## Nginx Stack link

[Download NGINX Stack Link](https://drive.google.com/open?id=1QmrLq5fYB4zx5KPVtUT-1wVy-99sSBw7)

`Tip:  
 Change the nginx and php daemon user to your user.
`

## Server Configuration [NGINX]
`Note: Remember to change '/opt/nginxstack-1.14.0-0' with your installed configuration`

```smartyconfig
$ mkdir -p /path/to/app/multi_tenancy/nginx_logs
$ cat /opt/nginxstack-1.14.0-0/nginx/conf/myconf/mt_demo_app.conf
# nginx config

server {

    listen 80;
    
    # wildcard for subdomains
    # Thus forcing all the subdomains to make use of this server block
    server_name mt_demo_app.com ~^(.*)\.mt_demo_app.com$;
    
    # `$1` represents the value captured fromt he regex above.
    # Thus giving us a detailed server log which is specific to each subdomain    
    access_log /path/to/app/multi_tenancy/nginx_logs/$1.access.log;
    access_log /path/to/app/multi_tenancy/nginx_logs/main.access.log;
    error_log /path/to/app/multi_tenancy/nginx_logs/all.error.log;
    
    root /path/to/app/multi_tenancy/public;
    
    # PHP FPM configuration.
    location ~* \.php$ {
        fastcgi_pass unix:/opt/nginxstack-1.14.0-0/php/var/run/www.sock;
        fastcgi_index index.php;
        fastcgi_split_path_info ^(.+\.php)(.*)$;
        include /opt/nginxstack-1.14.0-0/nginx/conf/fastcgi_params;
        fastcgi_read_timeout 3600;
        fastcgi_param HAL_MYSQL_BIN /opt/nginxstack-1.14.0-0/mysql/bin;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
    
    location / {
    
        index index.php index.html index.htm;
        try_files $uri $uri/ /index.php?$query_string;
    }
}
```

## Systemd Unit Configuration

If on Linux use this Unit file to auto start the server and manage

```smartyconfig
$ cat  /etc/systemd/system/nginx-stack.service 
[Unit]
Description=Nginx Stack Service
After=syslog.target network.target

[Service]
Type=forking
ExecStart=/opt/nginxstack-1.14.0-0/ctlscript.sh start
ExecReload=/opt/nginxstack-1.14.0-0/ctlscript.sh restart
ExecStop=/opt/nginxstack-1.14.0-0/ctlscript.sh stop

[Install]
WantedBy=multi-user.target
```

## Final Steps to get the App start working

Since we are performing `git clone` of the project few directories and configs are not set automatically.  
So this might be necessary.

```smartyconfig
$ mkdir -p bootstrap/cache
$ cp .env.example .env
$ mkdir -p storage/framework/{sessions,views,cache}
$ php artisan cache:clear
$ php artisan key:generate
```