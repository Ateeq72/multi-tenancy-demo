@extends('layouts.app_no_auth')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-danger" role="alert">
                            {{$tenant_name}} tenant not found!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
