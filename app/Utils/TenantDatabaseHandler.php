<?php

use Illuminate\Support\Str;

/**
 * Created by PhpStorm.
 * User: ateeq-ahmed
 * Date: 29/11/19
 * Time: 9:26 PM
 */
class TenantDatabaseHandler {

    /**
     * @param $tenantData
     */
    public function setTenantDatabaseConnection($tenantData) {

        \Config::set(['database.connections.'.$tenantData->domain_name => self::createTenantDBConfig($tenantData)]);

        \DB::setDefaultConnection($tenantData->domain_name);

        \Cache::setPrefix($tenantData->domain_name.'_'.env('APP_ENV'));

        \Config::set(['app.timezone' => $tenantData->timezone]);

        //Set Time Zone in the App
        \App::setLocale(config('app.timezone'));

        date_default_timezone_set(config('app.timezone'));

        \DB::reconnect();

    }

    public function unSetTenantDatabaseConnection($tenantData, $revertTo = 'mysql') {

        \DB::purge($tenantData->domain_name);

        $connections = config('database.connections');

        unset($connections[$tenantData->domain_name]);

        \Config::set(['database.connections' => $connections]);

        \DB::setDefaultConnection($revertTo);

        \Cache::setPrefix(env('CACHE_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_cache'));

        \Config::set(['app.timezone' => 'UTC']);

        //Set Time Zone in the App
        \App::setLocale(config('app.timezone'));

        date_default_timezone_set(config('app.timezone'));

        \DB::reconnect();
    }

    /**
     * @param $tenantData
     * @return array
     * @throws \Exception
     */
    public function createTenantDBConfig($tenantData){

        return $connection = [
            'driver' => 'mysql',
            'host' => $tenantData->db_host,
            'port' => $tenantData->db_port,
            'database' => $tenantData->db_name,
            'username' => $tenantData->db_user,
            'password' => $tenantData->db_password,
            'charset' => $tenantData->db_charset,
            'collation' => $tenantData->db_collation,
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ];
    }

}