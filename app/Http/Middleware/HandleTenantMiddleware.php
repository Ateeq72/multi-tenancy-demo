<?php

namespace App\Http\Middleware;

use App\Tenants;
use Closure;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HandleTenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $tenant = \Session::get('tenant');

        if ($tenant == null || $tenant == "")  return $next($request);

        $tenantData = Tenants::where('domain_name', $tenant)->first();

        if ($tenantData == null) return response()->view('tenant_not_found', ['tenant_name' => strtoupper($tenant)]);
//        if ($tenantData == null) throw new NotFoundHttpException();

        (new \TenantDatabaseHandler())->setTenantDatabaseConnection($tenantData);


        return $next($request);
    }


}
