<?php

namespace App\Console\Commands;

use App\Tenants;
use Illuminate\Console\Command;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateMT extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate_mt:run {--cmd=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Migrate In All Tenants';

    protected $cmd = "";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->setCmd();

        $this->runMigrationsInAllTenants();

    }

    private function setCmd()
    {

        $this->cmd = $this->option('cmd');

        if($this->cmd != '') $this->cmd = ':'.$this->cmd;
    }

    private function runMigrationsInAllTenants()
    {

        foreach (Tenants::get() as $tenantData) {

            $this->info('Connecting and Running in: `'.$tenantData->domain_name.'`');

            (new \TenantDatabaseHandler())->setTenantDatabaseConnection($tenantData);

            $this->line('');

            $this->call('migrate'.$this->cmd);

            $this->line('');

            (new \TenantDatabaseHandler())->unSetTenantDatabaseConnection($tenantData);

            $this->info('Done Running in '.$tenantData->domain_name);
            $this->info("\n");
        }

        $this->info('Running in `Main App`');

        $this->line('');

        $this->call('migrate'.$this->cmd);

        $this->info("\n");
    }
}
