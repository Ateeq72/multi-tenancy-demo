<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('domain_name', 255);
            $table->string('tenant_name', 255);
            $table->string('db_host', 255)->default('127.0.0.1');
            $table->string('db_name', 255);
            $table->string('db_user', 255);
            $table->string('db_password', 255);
            $table->string('db_port', 255)->default('3306');
            $table->string('db_charset', 255)->default('utf8');
            $table->string('db_collation', 255)->default('utf8_unicode_ci');
            $table->enum('timezone', ['Asia/Kolkata','Asia/Kuwait'])->default('Asia/Kolkata');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
