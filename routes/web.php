<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

$hosts = request()->getHost();

$pieces = explode('.',$hosts);
$domainName = env('DOMAIN_NAME');

if (isset($pieces[1]) && $domainName == $pieces[1]) {

    Session::put('tenant', $pieces[0]);

    Auth::routes();

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/home', 'HomeController@index')->name('home');
}
else {

    Route::get('/', function () {
        return view('welcome');
    });
}
